# AdGuard Home Cloudron App

This repository contains the Cloudron app package source for [AdGuard Home](https://adguard.com/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=com.adguard.home.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.adguard.home.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd adguard-home-app

cloudron build
cloudron install
```

## Notes

### DoH

There are two formats to send DNS over HTTPS:
    * Wire format - https://developers.cloudflare.com/1.1.1.1/encryption/dns-over-https/make-api-requests/dns-wireformat/ . You make queries like a DNS packet.

    curl -H 'accept: application/dns-message' 'https://dns.google/dns-query?dns=q80BAAABAAAAAAAAA3d3dwdleGFtcGxlA2NvbQAAAQAB'  | hexdump -c
    curl --output - -H 'accept: application/dns-json' 'https://adh.cloudron.club/dns-query?ct=application/dns-json&dns=q80BAAABAAAAAAAAB2V4YW1wbGUDY29tAAABAAE'
 

    * JSON format - https://developers.cloudflare.com/1.1.1.1/encryption/dns-over-https/make-api-requests/dns-json/ . People have not agreed on any specific JSON schema. From what I can tell, AdGuard Home does not support JSON format.

    curl -H 'accept: application/dns-json' 'https://cloudflare-dns.com/dns-query?name=example.com&type=A' | jq .

In AdGuard there are two ways to support DNS over HTTPS (https://github.com/AdguardTeam/AdGuardHome/wiki/Clients):

    * DNS-over-HTTPS: https://example.org/dns-query/my-client.
    * Since v0.108.0-b.18: https://my-client.example.org/dns-query (requires a wildcard certificate). NOTE: The URL ClientID has higher priority than the server-name ClientID. If you use both, only the URL ClientID is used.

To support above, we just enable `multiDomain` flag. This will allow the user to add `client1.domain.com` or `*.domain.com` and Cloudron will respond to DNS queries (proxied via nginx).

### DoT

Have to use kdig or curl --doh-url https://cloudflare-dns.com/dns-query https://github.com .

https://developers.cloudflare.com/1.1.1.1/encryption/dns-over-tls/

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if things are ok.

```
cd adguard-home-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

