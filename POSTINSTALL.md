This app is pre-setup with an admin account. The initial credentials are:
  
**Username**: admin<br/>
**Password**: changeme<br/>

See the [docs](https://docs.cloudron.io/apps/adguard-home) on how to change the admin password
and secure your installation.

