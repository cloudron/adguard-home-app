## About

AdGuard Home is a network-wide software for blocking ads & tracking. After you set it up, it'll cover ALL your home devices, and you don't need any client-side software for that.

It operates as a DNS server that re-routes tracking domains to a "black hole," thus preventing your devices from connecting to those servers. It's based on software we use for our public AdGuard DNS servers -- both share a lot of common code.

### Features

* Blocking ads and trackers
* Customizing blocklists
* Built-in DHCP server
* HTTPS for the Admin interface
* Encrypted DNS upstream servers (DNS-over-HTTPS, DNS-over-TLS, DNSCrypt)
* Cross-platform
* Running as a DNS-over-HTTPS or DNS-over-TLS server
* Blocking phishing and malware domains
* Parental control (blocking adult domains)
* Force Safe search on search engines
* Per-client (device) configuration
* Access settings (choose who can use AGH DNS)

