#!/usr/bin/env node

'use strict';

const fs = require('fs');
const c1 = fs.readFileSync(process.argv[2], { encoding: 'utf8' });
const c2 = fs.readFileSync('/app/code/isrg-root-x1-cross-signed.pem', { encoding: 'utf8' });
const index = c1.indexOf(c2);
if (index === -1) {
    process.stdout.write(c1);
} else {
    process.stdout.write(c1.slice(0, index) + c1.slice(index+c2.length));
}

