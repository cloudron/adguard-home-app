FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=AdguardTeam/AdGuardHome versioning=semver extractVersion=^v(?<version>.+)$
ARG ADGUARD_VERSION=0.108.0-b.63

RUN curl -L https://github.com/AdguardTeam/AdGuardHome/releases/download/v${ADGUARD_VERSION}/AdGuardHome_linux_amd64.tar.gz | tar -xz --strip-components 2 -f -

COPY AdGuardHome.yaml.template start.sh isrg-root-x1-cross-signed.pem remove-root-x1-cross-signed.js /app/code/

CMD [ "/app/code/start.sh" ]
