#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const crypto = require('crypto'),
    dns = require('dns/promises'),
    execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}
describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login() {
        await browser.get('https://' + app.fqdn);
        await browser.get('https://' + app.fqdn + '/login.html');
        await browser.wait(until.elementLocated(By.xpath('//input[@id="username1"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@id="username1"]')).sendKeys('admin');
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys('changeme');
        await browser.findElement(By.tagName('form')).submit();
        await browser.wait(until.elementLocated(By.xpath('//h1[text()="Dashboard"]')), TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//h1[text()="Dashboard"]')), TIMEOUT);
        await browser.sleep(4000);
        await browser.findElement(By.xpath('//a[@href="control/logout"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//input[@id="username1"]')), TIMEOUT);
    }

    async function checkDns() {
        const ip = execSync(`host ${app.fqdn}`, { encoding: 'utf8' }).split('\n')[0].trim().split(' ')[3];
        const resolver = new dns.Resolver();
        resolver.setServers([ ip ]);
        const result = await resolver.resolve('api.dev.cloudron.io', 'A');
        if (result.length !== 1) throw new Error(`Bad result: ${JSON.stringify(result)}`);
    }

    // https://developers.google.com/speed/public-dns/docs/secure-transports
    // install kdig from here - https://www.knot-dns.cz/download/
    async function checkDoT() {
        const output= execSync(`kdig -d +noall +answer @${app.fqdn} cloudron.io +tls-ca +tls-hostname=${app.fqdn}`, { encoding: 'utf8' });
        if (!output.includes('165.227.67.76')) throw new Error(`DoT is not working: ${output}`);
    }

    async function checkDoTClientId() {
        const clientId = crypto.randomBytes(4).toString('hex');
        const output= execSync(`kdig -d +noall +answer @${clientId}.${app.fqdn} cloudron.io +tls-ca +tls-hostname=${clientId}.${app.fqdn}`, { encoding: 'utf8' });
        if (!output.includes('165.227.67.76')) throw new Error(`DoT is not working: ${output}`);
    }

    async function checkDoH() {
        // https://support.opendns.com/hc/en-us/articles/360038463251-Querying-OpenDNS-using-DoH-for-developers-
        const output = execSync(`curl -H "accept: application/dns-message" 'https://${app.fqdn}/dns-query?dns=rmUBAAABAAAAAAAAB2NhcmVlcnMHb3BlbmRucwNjb20AAAEAAQ' | hexdump -C`, { encoding: 'utf8' });
        if (!output.includes('opendns.com')) throw new Error(`DoH is not working: ${output}`);
    }

    async function checkDoHClientId() {
        const clientId = crypto.randomBytes(4).toString('hex');
        console.log(`check DoH with ${clientId}`);
        // https://support.opendns.com/hc/en-us/articles/360038463251-Querying-OpenDNS-using-DoH-for-developers-
        execSync(`curl -H "accept: application/dns-message" 'https://${clientId}.${app.fqdn}/dns-query?dns=rmUBAAABAAAAAAAAB2NhcmVlcnMHb3BlbmRucwNjb20AAAEAAQ'`);
        await browser.get('https://' + app.fqdn + '/#logs');
        await browser.sleep(3000);
        await browser.wait(until.elementLocated(By.xpath(`//a[text()="${clientId}"]`)), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    // test if package works without alias domain (default setup)
    it('can check DoH', checkDoH);
    it('can check DoT', checkDoT);

    it('add alias', function () { execSync(`cloudron configure --location ${LOCATION} --app ${app.id} --alias-domains *.${LOCATION}`, EXEC_ARGS); });

    it('can login', login);
    it('can check DoH', checkDoHClientId);
    it('can logout', logout);
    it('can check dns', checkDns);
    it('can check DoT', checkDoT);
    it('can check DoT', checkDoTClientId);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('can check DoH', checkDoHClientId);
    it('can logout', logout);
    it('can check dns', checkDns);
    it('can check DoT', checkDoT);
    it('can check DoT', checkDoTClientId);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION} --alias-domains *.${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can check DoH', checkDoHClientId);
    it('can logout', logout);
    it('can check dns', checkDns);
    it('can check DoT', checkDoT);
    it('can check DoT', checkDoTClientId);

    it('move to different location', function () { execSync(`cloudron configure --location ${LOCATION}2  --alias-domains *.${LOCATION}2 --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can check DoH', checkDoHClientId);
    it('can logout', logout);
    it('can check dns', checkDns);
    it('can check DoT', checkDoT);
    it('can check DoT', checkDoTClientId);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.adguard.home.cloudronapp --location ${LOCATION} --alias-domains *.${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can login', login);
    it('can check DoH', checkDoHClientId);
    it('can logout', logout);
    it('can check dns', checkDns);
    it('can check DoT', checkDoT);
    it('can check DoT', checkDoTClientId);
    it('uninstall app', function () { execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS); });
});

