#!/bin/bash

set -eu

echo "=> App startup"

mkdir -p /run/adguard

if [[ ! -f /app/data/AdGuardHome.yaml ]]; then
    echo "=> Generating config on first run"

    cp /app/code/AdGuardHome.yaml.template /app/data/AdGuardHome.yaml

    password=$(htpasswd -nbB admin changeme | head -n1 | cut -f2 -d ":")

    yq eval -i ".users[0].name=\"admin\"" /app/data/AdGuardHome.yaml
    yq eval -i ".users[0].password=\"${password}\"" /app/data/AdGuardHome.yaml
fi

echo "=> Ensure default settings"
yq eval -i ".tls.server_name=\"${CLOUDRON_APP_DOMAIN}\"" /app/data/AdGuardHome.yaml
yq eval -i ".http.address=\"0.0.0.0:3000\"" /app/data/AdGuardHome.yaml
yq eval -i ".dns.use_private_ptr_resolvers=false" /app/data/AdGuardHome.yaml

if [[ -z "${DNS_TLS_PORT:-}" ]]; then
    yq eval -i ".tls.enabled=false" /app/data/AdGuardHome.yaml
else
    yq eval -i ".tls.enabled=true" /app/data/AdGuardHome.yaml
    yq eval -i ".tls.port_https=0" /app/data/AdGuardHome.yaml
    yq eval -i ".tls.allow_unencrypted_doh=true" /app/data/AdGuardHome.yaml
    # Pre 7.4, the wildcard cert had a "*" in it (instead of _). We can use this bug to detect if the cert has also the top level domain in it and not have minBoxVersion set.
    if [[ -f "/etc/certs/_.${CLOUDRON_APP_DOMAIN}.cert" ]]; then
        echo "=> Using wildcard certs for ClientID support"
        yq eval -i ".tls.private_key_path=\"/etc/certs/_.${CLOUDRON_APP_DOMAIN}.key\"" /app/data/AdGuardHome.yaml
        /app/code/remove-root-x1-cross-signed.js /etc/certs/_.${CLOUDRON_APP_DOMAIN}.cert > /run/adguard/_.${CLOUDRON_APP_DOMAIN}.cert
        yq eval -i ".tls.certificate_path=\"/run/adguard/_.${CLOUDRON_APP_DOMAIN}.cert\"" /app/data/AdGuardHome.yaml
    else
        echo "=> ClientID support disabled because of no wildcard cert"
        yq eval -i ".tls.private_key_path=\"/etc/certs/tls_key.pem\"" /app/data/AdGuardHome.yaml
        /app/code/remove-root-x1-cross-signed.js /etc/certs/tls_cert.pem > /run/adguard/tls_cert.pem
        yq eval -i ".tls.certificate_path=\"/run/adguard/tls_cert.pem\"" /app/data/AdGuardHome.yaml
    fi
fi

echo "=> Starting AdGuard Home"

# https://github.com/AdguardTeam/AdGuardHome/wiki/Configuration
exec /app/code/AdGuardHome --work-dir /app/data --no-check-update

